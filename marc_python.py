# Declaration variables
possibility = []

# Ajoute les valeurs acceptées pour le calcul dans le tableau possibility
for i in range(0, 10):
  possibility.append(str(i))

somme = 0
produit = 1
moyenne = 0
nb_num = 0

num = ''
# Tant que num pas egal a zero on redemande
while(num != '0'):
  num = input("Entrer une valeur: ")
  if num not in possibility:
    print("\a")
  # traitement
  elif num != '0':
    somme += int(num)
    produit = produit * int(num)
    nb_num += 1

# Si aucun nombre n'est entré
if nb_num > 0:
  moyenne = somme / nb_num
else:
  somme = 0
  produit = 0
  moyenne = 0 
  
# Affichage
print("Somme = ", somme)
print("Produit = ", produit)
print("Moyenne = ", moyenne)